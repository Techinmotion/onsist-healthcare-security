# Ransomware Attacks Show That Healthcare Must Take Cybersecurity Seriously #

While healthcare providers and healthcare industry vendors cannot afford to ignore HIPAA, a new threat has emerged and is poised to become much bigger: ransomware attacks on hospitals and healthcare providers that are not seeking to breach patient information but instead render it inaccessible until the organization pays a hefty ransom.

In just the past few weeks, the following major ransomware attacks on healthcare facilities have occurred:

In February 2016, hackers used a piece of ransomware called Locky to attack Hollywood Presbyterian Medical Center in Los Angeles, rendering the organization's computers inoperable. After a week, the hospital gave in to the hackers' demands and paid a $17,000.00 Bitcoin ransom for the key to unlock their computers.

In early March 2016, Methodist Hospital in Henderson, Kentucky, was also attacked using Locky ransomware. Instead of paying the ransom, the organization restored the data from backups. However, the hospital was forced to declare a "state of emergency" that lasted for approximately three days.

In late March, MedStar Health, which operates 10 hospitals and over 250 outpatient clinics in the Maryland/DC area, fell victim to a ransomware attack. The organization immediately shut down its network to prevent the attack from spreading and began to gradually restore data from backups. Although MedStar's hospitals and clinics remained open, employees were unable to access email or electronic health records, and patients were unable to make appointments online; everything had to go back to paper.
Likely, this is only the beginning. A recent study by the Health Information Trust Alliance found that 52% of U.S. hospitals' systems were infected by malicious software.

What is ransomware?

Ransomware is malware that renders a system inoperable (in essence, holding it hostage) until a ransom fee (usually demanded in Bitcoin) is paid to the hacker, who then provides a key to unlock the system. As opposed to many other forms of cyber attacks, which usually seek to access the data on a system (such as credit card information and Social Security numbers), ransomware simply locks the data down.

Hackers usually employ social engineering techniques - such as phishing emails and free software downloads - to get ransomware onto a system. Only one workstation needs to be infected for ransomware to work; once the ransomware has infected a single workstation, it traverses the targeted organization's network, encrypting files on both mapped and unmapped network drives. Given enough time, it may even reach an organization's backup files - making it impossible to restore the system using backups, as Methodist Hospital and MedStar did.

Once the files are encrypted, the ransomware displays a pop-up or a webpage explaining that the files have been locked and giving instructions on how to pay to unlock them (some MedStar employees reported having seen such a pop-up before the system was shut down). The ransom is nearly always demanded in the form of Bitcoin (abbreviated as BTC), an untraceable "cryptocurrency." Once the ransom is paid, the hacker promises, a decryption key will be provided to unlock the files.

Unfortunately, because ransomware perpetrators are criminals - and thus, untrustworthy to begin with - paying the ransom is not guaranteed to work. An organization may pay hundreds, even thousands of dollars and receive no response, or receive a key that does not work, or that does not fully work. For these reasons, as well as to deter future attacks, the FBI recommends that ransomware victims not cave in and pay. However, some organizations may panic and be unable to exercise such restraint.

Because of this, ransomware attacks can be much more lucrative for hackers than actually stealing data. Once a set of data is stolen, the hacker must procure a buyer and negotiate a price, but in a ransomware attack, the hacker already has a "buyer": the owner of the information, who is not in a position to negotiate on price.

Why is the healthcare industry being targeted in ransomware attacks?

There are several reasons why the healthcare industry has become a prime target for ransomware attacks. First is the sensitivity and importance of healthcare data. A company that sells, say, candy or pet supplies will take a financial hit if it cannot access its customer data for a few days or a week; orders may be left unfilled or delivered late. However, no customers will be harmed or die if a box of chocolates or a dog bed isn't delivered on time. The same cannot be said for healthcare; physicians, nurses, and other medical professionals need immediate and continuous access to patient data to prevent injuries, even deaths.

U.S. News & World Report points to another culprit: the fact that healthcare, unlike many other industries, went digital practically overnight instead of gradually and over time. Additionally, many healthcare organizations see their IT departments as a cost to be minimized, and therefore do not allocate enough money or human resources to this function:

According to the statistics by Office of National Coordinator for Health Information Technology, while only 9.4 percent of hospitals used a basic electronic record system in 2008, 96.9 percent of them were using certified electronic record systems in 2014.
This explosive growth rate is alarming and indicates that health care entities could not have the organizational readiness for adopting information technologies over such short period of time. Many of the small- or medium-sized health care organizations do not view IT as an integral part of medical care but rather consider it as a mandate that was forced on them by larger hospitals or the federal government. Precisely due to this reason, health care organizations do not prioritize IT and security technologies in their investments and thus do not allocate required resources to ensure the security of their IT systems which makes them especially vulnerable to privacy breaches.

What can the healthcare industry do about ransomware?

First, the healthcare industry needs a major shift in mindset: Providers must stop seeing information systems and information security as overhead costs to be minimized, realize that IT is a critical part of 21st century healthcare, and allocate the appropriate monetary and human resources to running and securing their information systems.

The good news is, since ransomware almost always enters a system through simple social engineering techniques such as phishing emails, it is fully possible to prevent ransomware attacks by taking such measures as:

Instituting a comprehensive organizational cyber security policy
Implementing continuous employee training on security awareness
Regular penetration tests to identify vulnerabilities
Onsist feels that it is much better to prevent a ransomware attack than to attempt to deal with one after it has occurred, especially in a healthcare environment, where lives are at stake should patient data become inaccessible. We offer full-service risk assessment services and Continuum GRC software to protect hospitals and other healthcare organizations. * [Onsist](https://www.onsist.com/healthcare-security/) cyber security. Con tact them to discuss your organization's cyber security needs and find out how we can help you prevent your facility from becoming the next victim of a ransomware attack.



